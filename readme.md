*PJMEN*
PassportJS, Javascript, MongoJS, ExpressJS, NodeJS base project.

Prerequisits:
- Install Mongo: https://docs.mongodb.com/manual/administration/install-community/
- Install Express Generator: `npm install express-generator -g`

Based On: https://www.djamware.com/post/58eba06380aca72673af8500/node-express-mongoose-and-passportjs-rest-api-authentication