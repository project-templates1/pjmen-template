var express = require('express');
var router = express.Router();
var passport = require('passport');

router.get('/profile', passport.authenticate('jwt', { session: false}), function(req, res, next) {
    console.log('req.user: '+req.user);
    res.render('profile', { user: req.user});
});

module.exports = router;