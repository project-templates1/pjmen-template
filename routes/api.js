const mongoose = require('mongoose');
const passport = require('passport');
const config = require('../config/database');
require('../config/passport')(passport);
const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const User = require('../models/user');
const Book = require('../models/book');

router.post('/signup', function(req, res) {
  if (!req.body.username || !req.body.password) {
    res.json({success: false, msg: 'Please pass username and password.'});
  } else {
    const newUser = new User({
      username: req.body.username,
      password: req.body.password,
      createdDate: new Date()
    });
    // save the user
    newUser.save(function(err) {
    	console.log('save, err',err);
      if (err) {
        return res.json({success: false, msg: err || 'Username already exists.'});
      }
      res.json({success: true, msg: 'Successful created new user. Please login.'});
    });
  }
});

router.post('/login', function(req, res) {
  User.findOne({
    username: req.body.username
  }, function(err, user) {
    if (err) throw err;

    if (!user) {
      res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
    } else {
      // check if password matches
      user.comparePassword(req.body.password, function (err, isMatch) {
        if (isMatch && !err) {
          // if user is found and password is right create a token
          const token = jwt.sign(user.toJSON(), config.secret);
          console.log('----===> token: '+token);

          // return the information including token as JSON
          res.json({success: true, token: 'JWT ' + token});
        } else {
          return res.status(401).send({success: false, msg: 'Authentication failed. Wrong password.'});
        }
      });
    }
  });
});

// router.post('/p-r-o-f-i-l-e', passport.authenticate('jwt', { session: false}), function(req, res) {
//   const token = getToken(req.headers);
//   console.log('------>>> token: '+token);
//   console.log({req});
//   if (token) {
//     console.log(req.body);
//     res.json({success: true, user});
//   } else {
//     return res.status(403).send({success: false, msg: 'Unauthorized.'});
//   }
// });

// router.post('/book', passport.authenticate('jwt', { session: false}), function(req, res) {
//   const token = getToken(req.headers);
//   if (token) {
//     console.log(req.body);
//     const newBook = new Book({
//       isbn: req.body.isbn,
//       title: req.body.title,
//       author: req.body.author,
//       publisher: req.body.publisher
//     });

//     newBook.save(function(err) {
//       if (err) {
//         return res.json({success: false, msg: 'Save book failed.'});
//       }
//       res.json({success: true, msg: 'Successful created new book.'});
//     });
//   } else {
//     return res.status(403).send({success: false, msg: 'Unauthorized.'});
//   }
// });

// router.get('/book', passport.authenticate('jwt', { session: false}), function(req, res) {
//   const token = getToken(req.headers);
//   if (token) {
//     Book.find(function (err, books) {
//       if (err) return next(err);
//       res.json(books);
//     });
//   } else {
//     return res.status(403).send({success: false, msg: 'Unauthorized.'});
//   }
// });

 function getToken(headers) {
  if (headers && headers.authorization) {
    const parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
}

module.exports = router;