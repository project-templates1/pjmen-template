var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

var UserSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    createdDate: {
        type: Date, 
        unique: false,
        required: false
    }
});

UserSchema.pre('save', function (next) {
    var user = this;
    console.log('user',user);
    if (this.isModified('password') || this.isNew) {
        console.log('this',this);
        bcrypt.genSalt(10, function (err, salt) {
            console.log('genSalt - err',err);
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, null, function (err, hash) {
                console.log('hash - err',err);

                if (err) {
                    return next(err);
                }
                user.password = hash;
                console.log('hash - next',next);
                console.log('hash - user',user);
                next();
            });
        });
    } else {
        console.log('save next');
        return next();
    }
});

UserSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('User', UserSchema);