function login(){
    const userElement = document.getElementById('username');
    const passwordElement = document.getElementById('password');
    const responseElement = document.getElementById('response');
    const user = userElement.value;
    const password = passwordElement.value;

    const sendObject = JSON.stringify({username: user, password: password});

    callout('POST','api/login',null,sendObject)
        .then((data)=>{
            const responseObject = JSON.parse(data);
            const token = responseObject.token;
            console.log(responseObject);
            if (token) {
                responseElement.innerHTML = token;
                localStorage.setItem('token', token);

                callout('GET','profile',token)
                    .then(data => {
                        console.log(data);
                        document.querySelector('html').innerHTML = data;
                    });
            } else {
              responseElement.innerHTML = "No token received";
            }
        });
}   

function signup(){
    const url = "http://localhost:3000/api/signup";
    const xhr = new XMLHttpRequest();
    const userElement = document.getElementById('username');
    const passwordElement = document.getElementById('password');
    const responseElement = document.getElementById('response');
    const user = userElement.value;
    const password = passwordElement.value;

    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    xhr.addEventListener('load', function() {
        console.log(this.response);
        const responseObject = JSON.parse(this.response);
        responseElement.innerHTML = JSON.stringify(responseObject.msg);
    });

    const sendObject = JSON.stringify({username: user, password: password});

    console.log('going to send', sendObject);

    xhr.send(sendObject);
}   

function callout(method,url,token,payload){
    return new Promise((resolve,reject) => {
        const baseURL = 'http://localhost:3000/';
        const xhr = new XMLHttpRequest();
        xhr.open(method, baseURL+url, true);

        xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        if (token) {
            xhr.setRequestHeader('Authorization', token);
        }

        xhr.addEventListener('load', function() {
            const response = this.response;
            console.log(response);
            resolve(response);
        });

        if (payload) {
            xhr.send(payload);
        } else {
            xhr.send();
        }
    });
}

export {
    login,
    signup
}