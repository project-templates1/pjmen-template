import { login, signup } from './scripts/auth.js';

document.getElementById('login-btn').onclick = login;
document.getElementById('signup-btn').onclick = signup;